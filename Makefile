all: run

start:
	docker-compose up -d

stop:
	docker-compose stop
	
logs:
	docker-compose logs -ft

restart:
	docker-compose restart
