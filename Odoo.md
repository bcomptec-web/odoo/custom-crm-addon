## Odoo (formely OpenERP, formely TinyERP)
- License: LGPLv3  
- Tech: Python, JavaScript, PostgreSQL  
- [Wikipedia](https://en.wikipedia.org/wiki/Odoo)
- [Code](https://github.com/odoo/odoo)
- [Community](https://www.odoo.com/page/community)  
- [Odoo 12 release notes](https://www.odoo.com/odoo-12-release-notes)  
- [Quinta Odoo analysis](https://quintagroup.com/cms/python/odoo)

## Comparison
- Editions:  
    - https://www.odoo.com/page/editions  
    - The free and open source community edition has limited features compared to the paid enterprise suite.  

## Apps (Addons)
- https://apps.odoo.com/apps?series=12.0
- https://github.com/odoo/odoo/tree/12.0/addons
- [Web Google Maps](https://apps.odoo.com/apps/modules/12.0/web_google_maps/)
    - https://github.com/gityopie/odoo-addons
    - https://www.youtube.com/channel/UCxZX2iwoBtn06WlEPYsNSWQ/videos
- [Web Google Maps Drawing](https://apps.odoo.com/apps/modules/12.0/web_google_maps_drawing/)

```
Using configuration file at /etc/odoo/odoo.conf 
addons paths: ['/var/lib/odoo/addons/12.0', '/mnt/extra-addons', '/usr/lib/python3/dist-packages/odoo/addons'] 
```

## Documentation
- [End user documentation](https://github.com/odoo/documentation-user)
- [Odoo development](https://odoo-development.readthedocs.io/en/latest/index.html)
- [Tutorials](https://www.odoo.com/documentation/master/tutorials.html)
- [Open Source Python CRM Tutorial](https://www.tomordonez.com/open-source-python-crm-tutorial.html) 
- [How do I configure Odoo CRM so users must create 'Leads' first, not 'Opportunities'?](https://www.odoo.com/forum/help-1/question/how-do-i-configure-odoo-crm-so-users-must-create-leads-first-not-opportunities-131605)

#### Download
- https://www.odoo.com/page/download
- [Odoo Nightly builds](http://nightly.odoo.com/)
- [Arch](https://aur.archlinux.org/packages/odoo/)

#### Install
- [Runbot](http://runbot.odoo.com/runbot)
- https://www.odoo.com/documentation/master/setup/install.html

#### Upgrade
- [OpenUpgrade](https://doc.therp.nl/openupgrade/)  
OpenUpgrade aims to provide an Open Source upgrade path for Odoo.   
This is a community initiative, as the open source version of Odoo does not support migrations from one major release to another.  
Instead, migrations are part of a support package sold by Odoo SA. 

#### Docker
- https://hub.docker.com/_/odoo/
- https://github.com/docker-library/docs/tree/master/odoo
- https://github.com/odoo/docker
- [Odoo 12 in Docker Container - How to run Odoo version 12 with Docker in a container](https://unkkuri.com/blog/unkkuri-blog-1/post/odoo-12-in-docker-container-25)

### Videos
- [Odoo YouTube channel](https://www.youtube.com/user/OpenERPonline/playlists)
- [Odoo App Tour](https://www.youtube.com/playlist?list=PL1-aSABtP6AArcQlEsAh903umc_nH9vxw)
- [Odoo CRM Webinars in English](https://www.youtube.com/playlist?list=PL1-aSABtP6ACpuUQrykRn9CSaxVZF1nF_)
- [Odoo Webinars in English](https://www.youtube.com/playlist?list=PL1-aSABtP6ACQ31opg84k9C0951aThI5W)

### Modules

#### [CRM](https://www.odoo.com/page/crm)  
- Track leads, close opportunities and get accurate forecasts.  
- Capture leads using a variety of methods such as entering manually, mass importing, incoming emails  

- [CRM documentation](https://www.odoo.com/documentation/user/12.0/crm.html)  
- [CRM addon code](https://github.com/odoo/odoo/tree/12.0/addons/crm)

Pipeline:  
    - [Manage lost opportunities](https://www.odoo.com/documentation/user/12.0/crm/pipeline/lost_opportunities.html)  

- https://www.odoo.com/page/lead-automation  
- [Get organized by planning activities](https://www.odoo.com/documentation/user/12.0/discuss/plan_activities.html)

- Import data
    - [How to import data into Odoo](https://www.odoo.com/documentation/user/12.0/general/base_import/import_faq.html)  
    - [How to adapt an import template](https://www.odoo.com/documentation/user/12.0/general/base_import/adapt_template.html)  

#### [Sales](https://www.odoo.com/page/sales)
From quotations to invoices

#### Discuss
Chat, mail gateway and private channels

#### Calendar
   - [Synchronize Google Calendar with Odoo](https://www.odoo.com/documentation/user/12.0/crm/optimize/google_calendar_credentials.html)  

#### [Email Marketing](https://www.odoo.com/page/email-marketing)
Design, send and track emails  
- [Odoo Email Marketing (Tour)](https://www.youtube.com/watch?v=pBX9t7L0PuA)
- [Odoo Mass Mailing](https://github.com/odoo/odoo/tree/12.0/addons/mass_mailing)

#### [Marketing Automation](https://www.odoo.com/page/marketing-automation)

## Concepts and terms
### Lead
Potential customer with whom you have not establishsed a relationship.  
Using leads is an optional extra step to filter spam/low quality prospects.  

Add a qualification step before creating an opportunity  
Use leads if you need a qualification step before creating an opportunity or a customer.   
It can be a business card you received, a contact form filled in your website, or a file of unqualified prospects you import, etc.  
Once qualified, the lead can be converted into a business opportunity and/or a new customer in your address book.  

### Opportunity
Confirmed interest. Contact created.  

### Fields
https://github.com/odoo/odoo/blob/12.0/odoo/fields.py

### Widgets
- [Form widgets for many2many fields in Odoo](http://ludwiktrammer.github.io/odoo/form-widgets-many2many-fields-options-odoo.html)
- [Form widgets for many2one fields in Odoo](http://ludwiktrammer.github.io/odoo/form-widgets-many2one-fields-options-odoo.html)
