#!/usr/bin/env bash

set -uo pipefail

# Import counties
# ../odoo_csv_import/odoo_import_thread.py --sep="," --quote="\"" --config "../odoo_csv_import/connection.conf" --model="res.country.state" --file "../odoo/addons/l10n_ro/data/res.country.state.csv"

# all caens: '*400000*.xlsx'
# single caen: '*6399*400000*.xlsx'

find . -name '*400000*.xlsx' -print0 | sort -z |
    while IFS= read -r -d $'\0' filepath; do 
        county="$(dirname $filepath | sed "s|^\./||")"

        filename=$(basename -- "$filepath")
        extension="${filename##*.}"
        filename="${filename%.*}"

        csvfile="$county/$filename.csv"
        echo "Converting County: $county, File: $csvfile"
        > "$csvfile"
        xlsx2csv/xlsx2csv.py -s 1 -d 'tab' -q "all" $filepath "$csvfile"
        number_of_columns=$(awk -F'\t' 'END {print NF}' "$csvfile")
        echo "[$county] Number of rows: $(wc -l $csvfile)"
        caen=$(echo "$csvfile" | grep -Po '([0-9]+)' | xargs | cut -d' ' -f1)

        # Input file missing locality column
        if [[ "$number_of_columns" -ne 9 ]]; then
            regs=()
            while read -r registration_number
            do
                # grep -Phr "${registration_number}" firme_neradiate | cut -d'^' -f6 | cut -d',' -f1 | sed 's/Sat |Municipiul //g'
                city=$(grep -Phr "${registration_number}" firme_neradiate | cut -d'^' -f6 | cut -d',' -f1 | sed -re 's/Municipiul |Sat |Loc. |Comuna |//g')
                if [[ -z "$city" ]]; then
                    city="$county"
                fi
                regs+=("\"$city\"")
            done < <(gawk -F$'\t' 'NR > 1 { print $3}' "$csvfile" | tr -d '"')

            echo -e "$(head -n 1 $csvfile)\t\"city\"" > "${csvfile}.tmp"
            paste -d$'\t' <(tail +2 "$csvfile") <(printf '%s\n' "${regs[@]}") >> "${csvfile}.tmp"
            mv "${csvfile}.tmp" "$csvfile"
        else
            gawk -i inplace -F$'\t' '{ gsub(/"/, "", $8); split($8, words, " "); city=""; for(i=1;i<=length(words);i++){ c=toupper(substr(words[i],1,1)) tolower(substr(words[i],2)); if (i > 1) { city=city " " c } else { city=c }; }; print $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6 "\t" $7 "\t" $9 "\t" "\"" city "\"" }' "$csvfile"
        fi

        industry="IT&C"
        case "$caen" in
            5829)
            caen="5829 (activități de editare a altor produse software)"
            ;;
            6201)
            caen="6201 (activități de realizare a soft-ului la comandă)"
            ;;
            6202)
            caen="6202 (activități de consultanță în tehnologia informației)"
            ;;
            6203)
            caen="6203 (activități de management - gestiune și exploatare - a mijloacelor de calcul)"
            ;;
            6209)
            caen="6209 (alte activități de servicii privind tehnologia informației)"
            ;;
            6311)
            caen="6311 (prelucrarea datelor, administrarea paginilor web și activități conexe)"
            ;;
            6312)
            caen="6312 (activități ale portalurilor web)"
            ;;
            7112)
            industry="Engineering"
            caen="7112 (activități de inginerie și consultanță tehnică legate de acestea)"
            ;;
            6399)
            caen="6399 (Alte activitati de servicii informationale n.c.a.)"
            ;;
            *)
            echo "Unknown caen code: $caen"
            exit 1
            ;;
        esac

        case "$county" in
            Arges)
                county="Argeș"
            ;;
            Bucuresti)
                county="București"
            ;;
            Bacau)
                county="Bacău"
            ;;
            Bistrita-nasaud)
                county="Bistrița-Năsăud"
            ;;
            Braila)
                county="Brăila"
            ;;
            Botosani)
                county="Botoșani"
            ;;
            Brasov)
                county="Brașov"
            ;;
            Buzau)
                county="Buzău"
            ;;
            Calarasi)
                county="Călărași"
            ;;
            Caras-severin)
                county="Caraș Severin"
            ;;
            Constanta)
                county="Constanța"
            ;;
            Dambovita)
                county="Dâmbovița"
            ;;
            Galati)
                county="Galați"
            ;;
            Ialomita)
                county="Ialomița"
            ;;
            Iasi)
                county="Iași"
            ;;
            Mehedinti)
                county="Mehedinți"
            ;;
            Maramures)
                county="Maramureș"
            ;;
            Mures)
                county="Mureș"
            ;;
            Neamt)
                county="Neamț"
            ;;
            Salaj)
                county="Sălaj"
            ;;
            Timis)
                county="Timiș"
            ;;
            Valcea)
                county="Vâlcea"
            ;;
            Satu-mare)
                county="Satu Mare"
            ;;
        esac
        
        gawk -i inplace -v user_id="Emanuel" -v industry="$industry" -v country="Romania" -v county="$county" -v caen="$caen" -F$'\t' '{ printf("%s\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t%s\t%s\n", $2, caen, county, country, industry, user_id, $1, $0); }' "$csvfile"
        
        sed -i '1s/.*/"id"\t"x_caen"\t"state_id"\t"country_id"\t"x_industry_id"\t"user_id"\t"name"\t"partner_name"\t"x_identification_number"\t"x_registration_number"\t"x_turnover"\t"x_gross_profit"\t"x_net_profit"\t"x_number_of_employees"\t"x_financial_report_url"\t"city"/' "$csvfile"

        echo "Importing County: $county, File: $csvfile"
        ../odoo_csv_import/odoo_import_thread.py -s$'\t' --quote="\"" --config "../odoo_csv_import/connection.conf" --model=crm.lead --file "$csvfile"
    done
