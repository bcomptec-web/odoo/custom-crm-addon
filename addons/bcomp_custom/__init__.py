# -*- coding: utf-8 -*-
from . import controllers
from . import models
from . import wizard

from logging import getLogger
_logger = getLogger(__name__)


def uninstall_hook(cr, registry):
    _logger.debug("Uninstalling BComp Custom module")
    pass
