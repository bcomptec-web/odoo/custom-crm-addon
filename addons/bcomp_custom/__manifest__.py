# -*- coding: utf-8 -*-
{
    'name': 'BComp Custom',
    'version': '12.0.1.0.0',
    'author': 'BComp...tec',
    'license': 'AGPL-3',
    'maintainer': 'BComp...tec',
    'support': '',
    'category': 'CRM',
    'description': """
BComp Custom
============

Custom modules.
""",
    'depends': [
        'base', 'base_setup', 'bus', 'crm'
    ],
    'data': [
        'wizard/email_template_preview_custom_view.xml',
    ],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
