# -*- coding: utf-8 -*-
import logging
from odoo.api import Environment
from odoo import SUPERUSER_ID
from odoo import http
from odoo.http import request, Response
from odoo import registry as registry_get

_logger = logging.getLogger(__name__)


# https://www.odoo.com/documentation/12.0/reference/http.html
# https://doc.odoo.com/trunk/web/web_controllers
# https://werkzeug.palletsprojects.com/en/0.15.x/routing/?
# https://www.odoo.com/documentation/12.0/setup/deploy.html#dbfilter
# https://www.odoo.com/documentation/12.0/reference/cmdline.html
class CustomBCompController(http.Controller):

    @http.route('/hello', type="http", auth='none')
    def say_hello(self, name):
        return "<h1>Hello {}</h1>".format(name)

    @http.route('/lead/mark_activity_as_done', type='http', auth='none', methods=['GET'])
    def crm_lead_mark_activity_as_done(self, res_id, activity_type_xml_id, summary, feedback):
        res_id = int(res_id)

        # First Email
        # Feedback Email
        _logger.info("[Mark activity as done] Lead id: %d, Activity type: %s, Summary: %s, Feedback: %s",
                     res_id, activity_type_xml_id, summary, feedback)

        response_content = False

        # Once done activities are unlinked and a message is posted.
        # This message has a new activity_type_id field that indicates the activity linked to the message
        try:
            registry = registry_get('db')
            with registry.cursor() as cr:
                env = Environment(cr, SUPERUSER_ID, {})
                activity_type_id = env['ir.model.data'].xmlid_to_res_id(activity_type_xml_id)
                domain = [
                    '&', '&', '&',
                    ('res_model', '=', 'crm.lead'),
                    ('res_id', '=', res_id),
                    ('activity_type_id', '=', activity_type_id),
                    ('summary', '=', summary)
                ]
                activities = env['mail.activity'].search(domain)
                if activities:
                    _logger.info("Found activities")
                    activities.action_feedback(feedback=feedback)
                    response_content = True
                else:
                    _logger.warning("No activities found! res_id=%d, activity_type_id=%d, summary=%s",
                                    res_id, activity_type_id, summary)
        except Exception:
            _logger.exception("Could not mark activity as done")

        if not response_content:
            return request.not_found()

        return request.make_response(str(response_content))
