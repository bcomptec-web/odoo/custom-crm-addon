import logging
# from datetime import datetime, timedelta, date
# from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, Lead

_logger = logging.getLogger(__name__)


class CrmLead(models.Model):

    @api.multi
    @api.returns('self', lambda value: value.id)
    def action_duplicate_lead_custom(self, default=None):
        self.ensure_one()

        # set default value in context, if not already set (Put stage to 'new' stage)
        context = dict(self._context)
        context.setdefault('default_type', self.type)
        context.setdefault('default_team_id', self.team_id.id)

        # Set date_open to today if it is an opp
        default = default or {}
        default['date_open'] = fields.Datetime.now() if self.type == 'opportunity' else False

        # Do not assign to an archived user
        if not self.user_id.active:
            default['user_id'] = False

        return super(Lead, self.with_context(context)).copy(default=default)