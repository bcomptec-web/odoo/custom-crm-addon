import logging
# from datetime import datetime, timedelta, date
# from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, Lead

_logger = logging.getLogger(__name__)


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    turtle_ids = fields.Many2many('x_turtle', 'crm_lead_turtle_rel', 'lead_id', 'turtle_id',
                                  string='Turtles',
                                  help="Turtles all the way down.")

    @api.multi
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        self.ensure_one()

        # set default value in context, if not already set (Put stage to 'new' stage)
        context = dict(self._context)
        context.setdefault('default_type', self.type)
        context.setdefault('default_team_id', self.team_id.id)

        # Set date_open to today if it is an opp
        default = default or {}
        default['date_open'] = fields.Datetime.now() if self.type == 'opportunity' else False

        # Do not assign to an archived user
        if not self.user_id.active:
            default['user_id'] = False

        return super(Lead, self.with_context(context)).copy(default=default)


class Turtle(models.Model):
    _name = "x_turtle"
    _description = "Turtles"

    name = fields.Char('Name', required=True, translate=True)
    color = fields.Integer('Color Index')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Turtle name already exists !"),
    ]
