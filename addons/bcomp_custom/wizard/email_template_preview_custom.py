# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo import registry as registry_get
import logging

_logger = logging.getLogger(__name__)


class TemplatePreviewCustom(models.TransientModel):
    _inherit = "mail.template"
    _name = "email_template.preview_custom"
    _description = "Email Template Preview Custom"

    @api.model
    def _get_records(self):
        """ Return Records of particular Email Template's Model """
        template_id = self._context.get('template_id')
        default_res_id = self._context.get('default_res_id')

        if not template_id:
            return []

        template = self.env['mail.template'].browse(int(template_id))

        records = self.env[template.model_id.model].search([], order="id desc", limit=10)
        records |= records.browse(default_res_id)

        return records.name_get()

    @api.model
    def default_get(self, fields):
        result = super(TemplatePreviewCustom, self).default_get(fields)

        if 'res_id' in fields and not result.get('res_id'):
            records = self._get_records()
            result['res_id'] = records and records[0][0] or False  # select first record as a Default

        return result

    res_id = fields.Integer('Resource Id')

    @api.onchange('res_id')
    @api.multi
    def on_change_res_id(self):
        if not self.res_id:
            return {}

        mail_values = {}

        if self._context.get('template_id'):
            template = self.env['mail.template'].browse(self._context['template_id'])
            self.name = template.name
            mail_values = template.generate_email(self.res_id)

        is_followup_preview = False
        if self._context['template_id'] == 14:
            is_followup_preview = True

        date = None
        if is_followup_preview:
            mail_activity_type_id = self.env['ir.model.data'].xmlid_to_res_id('mail.mail_activity_data_email')

            registry = registry_get('db')
            with registry.cursor() as cr:
                cr.execute('''SELECT to_char(date, 'DD.MM.YYYY') FROM public.mail_compose_message 
                    WHERE model = 'crm.lead'
                    AND res_id = %s
                    AND mail_activity_type_id = %s
                    AND body ilike %s
                ''', (self.res_id, mail_activity_type_id, '%<span>First Email</span>%'))
                result = cr.fetchone()
                if result:
                    date = result[0]

            if date:
                _logger.info("Date %s", date)
            else:
                _logger.info("Can't find mail composition to find first contact email date ~ res_id: %s, "
                             "mail_activity_type_id: %d", self.res_id, mail_activity_type_id)

        for field in ['subject', 'body_html']:
            value = mail_values.get(field, False)
            if is_followup_preview and field == 'body_html':
                if date:
                    value = value.replace('{{date}}', date)
                else:
                    value = value.replace(' from {{date}}', '')
            setattr(self, field, value)
