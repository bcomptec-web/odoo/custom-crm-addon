#!/bin/bash

# crontab 
# Minute   Hour   Day of Month       Month          Day of Week        Command    
# 40 17 * * MON-FRI $HOME/Odoo_Backups/backup_odoo.sh 

PGPASSWORD=odoo /usr/bin/pg_dump --format=c --compress=9 --encoding=UTF-8 -n public --verbose --host=localhost --port=5432 -U odoo db > $HOME/Odoo_Backups/odoo-dump-db-$(date "+%Y%m%d%H%M").backup
